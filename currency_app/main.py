from fastapi import FastAPI
from currency_app import models, database

from currency_app.api import api_router

app = FastAPI()

models.Base.metadata.create_all(bind=database.engine)

app = FastAPI()

app.include_router(api_router)
    


