

from fastapi import APIRouter
from currency_app.routes import register, auth, currency, token, account

api_router = APIRouter()


api_router.include_router(register.router)
api_router.include_router(auth.router)
api_router.include_router(currency.router)
api_router.include_router(token.router)
api_router.include_router(account.router)
