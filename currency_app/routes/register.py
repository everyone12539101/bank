
from fastapi import APIRouter
from currency_app import user_app, schemas_app, models, database, currency, token as token_modul
from fastapi import FastAPI, Depends, HTTPException, status, Header
from sqlalchemy.orm import Session

router = APIRouter()


@router.post("/register/")
def register_user_endpoint(user: schemas_app.UserCreate, db: Session = Depends(database.get_db)):
    return user_app.register_user(db=db, user=user)