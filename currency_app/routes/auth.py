
from fastapi import APIRouter
from fastapi.security import OAuth2PasswordRequestForm
from currency_app import user_app, schemas_app, models, database, currency, token as token_modul
from fastapi import FastAPI, Depends, HTTPException, status, Header
from sqlalchemy.orm import Session

router = APIRouter()


@router.post("/auth/")
async def login_user_endpoint(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(database.get_db)):
    user = user_app.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    
    access_token = token_modul.create_tokens(user)

    access_token=access_token

    return token_modul.return_headers(access_token)