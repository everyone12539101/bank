from fastapi import APIRouter, Depends, HTTPException, status
from currency_app import user_app, schemas_app, models, database, currency, token as token_modul
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from fastapi.security import APIKeyHeader
from datetime import datetime, timedelta
import json
from dotenv import load_dotenv
import os


dotenv_path = os.path.join(os.path.dirname(__file__), 'settings.env')
load_dotenv(dotenv_path)

SECRET_KEY = os.getenv('SECRET_KEY')
ALGORITHM = os.getenv('ALGORITHM')

header_scheme = APIKeyHeader(name="authorization")

router = APIRouter()

@router.post("/create_account/")
def create_account(data_carrency: schemas_app.CreateAccount, db: Session = Depends(database.get_db),authorization: str = Depends(header_scheme)):
    # Проверяем, предоставлен ли заголовок Authorization
    if authorization is None:
        raise HTTPException(status_code=401, detail="Authorization header is missing")

    # Извлекаем токен из заголовка.
    token_type, _, access_token = authorization.partition(' ')
    if token_type.lower() != "bearer" or not access_token:
        raise HTTPException(status_code=403, detail="Invalid authorization header format")
    
    try:
        token_modul.check_tokens(db, access_token)
        
        find_currency = currency.get_currency_by_code(data_carrency.code.upper(), db)
        if find_currency is None:
            raise HTTPException(status_code=404, detail="Currency not found")

        return create_account(db, access_token, find_currency)
    except HTTPException as e:
        return {"error": "Token expired, please log in again"}
    

@router.post("/deposit/")
def deposit_account(data_carrency: schemas_app.DepositAccount, db: Session = Depends(database.get_db),authorization: str = Depends(header_scheme)):
    # Проверяем, предоставлен ли заголовок Authorization
    if authorization is None:
        raise HTTPException(status_code=401, detail="Authorization header is missing")

    # Извлекаем токен из заголовка.
    token_type, _, access_token = authorization.partition(' ')
    if token_type.lower() != "bearer" or not access_token:
        raise HTTPException(status_code=403, detail="Invalid authorization header format")
    
    try:
        token_modul.check_tokens(db, access_token)
        
        account = top_up(db, data_carrency.id_account, data_carrency.amount)

        return {"message": "Account successfully topped up", "new_balance": account.balance}
    
    except HTTPException as e:
        return {"error": "Token expired, please log in again"}


@router.get("/get_users_account/")
def get_users_account(db: Session = Depends(database.get_db),authorization: str = Depends(header_scheme)):
    # Проверяем, предоставлен ли заголовок Authorization
    if authorization is None:
        raise HTTPException(status_code=401, detail="Authorization header is missing")

    # Извлекаем токен из заголовка.
    token_type, _, access_token = authorization.partition(' ')
    if token_type.lower() != "bearer" or not access_token:
        raise HTTPException(status_code=403, detail="Invalid authorization header format")
    
    try:
        token_modul.check_tokens(db, access_token)
        
        result = find_user(db, access_token)

        if result is None:
            return {"error": "Проблема при поиске пользователя"}

        user_find, user = result  # Теперь безопасно распаковываем, зная что result не None

        if not user_find:
            return {"error": "Пользователь не найден"}

        return get_accounts(db, user)
    except HTTPException as e:
        return {"error": "Token expired, please log in again"}


def get_accounts(db: Session, user: models.User):
    # Получение аккаунтов пользователя
    accounts = db.query(models.Account).filter(models.Account.user_id == user.id, models.Account.deleted == False).all()
    
    # Подготовка данных для JSON
    accounts_data = [{"id_account": str(account.id), "currency_id": account.currency_id} for account in accounts]
    
    # Возвращение данных в JSON-совместимом формате
    return jsonable_encoder(accounts_data)


def top_up(db, account_id: str, amount: float):
    account = db.query(models.Account).filter(models.Account.id == account_id).first()
    if not account:
        raise HTTPException(status_code=404, detail="Account not found")
    
    account.balance += amount
    db.commit()
    return account


def create_account(db: Session, access_token: str, id_currency: int):
    result = find_user(db, access_token)

    if result is None:
        return {"error": "Проблема при поиске пользователя"}

    user_find, user = result  # Теперь безопасно распаковываем, зная что result не None

    if not user_find:
        return {"error": "Пользователь не найден"}

    # Создание нового аккаунта
    new_account = models.Account(
        user_id=user.id,
        currency_id=id_currency
    )
    
    # Добавление аккаунта в сессию и коммит изменений1
    db.add(new_account)
    db.commit()
    
    # Возвращаем информацию о созданном аккаунте
    return {"message": "Аккаунт успешно создан", "success": True}


def find_user(db: Session, token: str):
    access_payload = token_modul.verify_token(token, SECRET_KEY, [ALGORITHM])
    
    if access_payload is None:
        return False, None

    if not access_payload or access_payload.get("exp") < datetime.utcnow().timestamp():
        return False, None

    user_id = access_payload.get("id")
    user = models.User.get_by_id(db, user_id)

    if user is None:
        return False, None

    return True, user  # Возвращаем True и пользователя, если он найден


    
    