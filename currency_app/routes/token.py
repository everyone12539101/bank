from fastapi import Depends, HTTPException, status, Header
from fastapi import APIRouter
from sqlalchemy.orm import Session
from currency_app import database, token as token_modul
from fastapi.responses import JSONResponse
from fastapi.security import APIKeyHeader


router = APIRouter()


header_scheme = APIKeyHeader(name="authorization")


@router.get("/token_upd/", response_class=JSONResponse)
def token_upd(key: str = Depends(header_scheme), db: Session = Depends(database.get_db)):

    if key is None:
        raise HTTPException(status_code=401, detail="Authorization header is missing")

    token_type, _, access_token = key.partition(' ')
    if token_type.lower() != "bearer" or not access_token:
        raise HTTPException(status_code=401, detail="Invalid authorization header format")

    tokens = token_modul.check_tokens(db, access_token)

    return token_modul.return_headers(tokens)