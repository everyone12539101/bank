from fastapi import APIRouter
from currency_app import user_app, schemas_app, models, database, currency, token as token_modul
from fastapi import FastAPI, Depends, HTTPException, status, Header
from sqlalchemy.orm import Session
from fastapi.security import APIKeyHeader

header_scheme = APIKeyHeader(name="authorization")

router = APIRouter()

@router.post("/post_currencies/")
def post_currency(data_carrency: schemas_app.Carrency, db: Session = Depends(database.get_db),authorization: str = Depends(header_scheme)):
    # Проверяем, предоставлен ли заголовок Authorization
    if authorization is None:
        raise HTTPException(status_code=401, detail="Authorization header is missing")

    # Извлекаем токен из заголовка.
    token_type, _, access_token = authorization.partition(' ')
    if token_type.lower() != "bearer" or not access_token:
        raise HTTPException(status_code=403, detail="Invalid authorization header format")
    
    try:
        token_modul.check_tokens(db, access_token)

        return currency.save_currencies(db, data_carrency)
    except HTTPException as e:
        return {"error": "Token expired, please log in again"}
    

@router.get("/currencies/")
def create_currencies(db: Session = Depends(database.get_db)):
    return currency.currencies_list(db)